
#About
Mix Table for Vue.js 2

# Installation

Install via NPM

```js

npm install v-mix-table

```

# Example

```js

<mix-table :data="data" ref="mixtable" @mixtable:fetch="fetch">
    <mix-table-toolbar slot="mix-table-toolbar" style="width:100%">
      <div class="form-group text-left">
        <button class="btn btn-outline btn-primary" v-on:click="add">Add new</button>
      </div>
    </mix-table-toolbar>


    <mix-table-column data-field="_id" type="checkbox" width="40px" class="text-center" dataClass="text-center" label="#"></mix-table-column>
    <mix-table-column data-field="www" label="Host"></mix-table-column>
    <mix-table-column data-field="vf" label="Verified" width="120px" class="text-center" dataClass="text-center"></mix-table-column>
    <mix-table-column data-field="df" label="Default" width="120px" class="text-center" dataClass="text-center" :format="isDefault"></mix-table-column>
    <mix-table-column data-field="_id" width="130px" label="Actions" type="slot" target="actions" class="text-center" dataClass="text-center"></mix-table-column>

    <template slot="actions" scope="props">

          <button class="btn btn-xs btn-outline btn-success" v-on:click="action('verify', props.value,  props.row, props.index)"><i class="fa fa-check-circle-o"></i></button>
          <button class="btn btn-xs btn-outline btn-info" v-on:click="action('edit', props.value,  props.row, props.index)"><i class="fa fa-edit"></i></button>
          <button class="btn btn-xs btn-outline btn-danger" v-on:click="action('delete', props.value,  props.row, props.index)"><i class="fa fa-remove"></i></button>
    </template>

  </mix-table>

```
