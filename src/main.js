// import Vue from 'vue'
// import Column from './column.vue'
// import Table from './table.vue'
// import Toolbar from './toolbar.vue'

// Vue.component('mix-table-toolbar', Toolbar)
// Vue.component('mix-table-column', Column)
// Vue.component('mix-table', Table)
// export default { Table }

import * as components from './components';


const MixTablePlugin = {
    install: function (Vue) {
        if (Vue.__mixtable_installed) {
            return;
        }

        Vue.__mixtable_installed = true;

        // Register components
        for (var component in components) {
            Vue.component(component, components[component]);
        }        
    }
};

if (typeof window !== 'undefined' && window.Vue) {
    window.Vue.use(MixTablePlugin);
}

export default MixTablePlugin;
